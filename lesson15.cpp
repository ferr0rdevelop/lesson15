#include <iostream>

/* 
    The method print even numbers
    if variable n is divisible by zero without remainder
    else print  odd numbers.
*/
void numberPrint()
{
    int n = 45;
    int i = 0;
    if (n % 2 != 0)
    {
        i = 1;
    }
    for (i; i <= n; i += 2)
    {
        std::cout << i << ' ';
    }
}

int main()
{
    // Print number from 0 to n.
    const int n = 23;
    for (int i = 0; i <= n; i++)
    {
        std::cout << i << ' ';
    }
    // Print new line.
    std::cout << std::endl;

    numberPrint();
}